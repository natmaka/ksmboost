# ksmboost

For Linux.  Temporarily boosts ksmd (KSM optimization).

Works on Debian 11.4 stable.

## Description

Launch the script.  It tweaks ksmd parameters in order for it to be more active until a gain is reached or a condition (given amount of full scans  or timeout) is satisfied.

## Installation

Copy the ksmboost shell script in the root's account PATH.
Launch it when useful (for example after many similar VMs were started and reached ready-to-serve state).

## Usage

``` shell
ksmboost```

Options:
- "-f F" Where F is the max amount of full scans (default value: 1).
- "-t T" Where T is a timeout in seconds (default value: 60)
- "-p P" Where P is the amount of pages to scan during a step (pages_to_scan)
- "-s S" Where S is the duration, in ms, of each pause between 2 steps (sleep_millisecs)
- "-q" Quiet
